<?php
	session_start();

	if (!empty($_POST['app'])) {
		global $app;
		$app = $_POST['app'];

		$_SESSION['senhaCript'] = $app['senhaCript'];
		$_SESSION['textoCript'] = $app['textoCript'];
		$_SESSION['senhaDecript'] = $app['senhaDecript'];
		$_SESSION['textoDeript'] = $app['textoDeript'];
	}

	if(isset($_POST['btnCript'])){
		if($app['senhaCript'] == ""){
			echo
            "<script>window.alert('Informe Senha Para Criptografia')          
            	window.location.replace('./');      
            </script>";
		}else if($app['textoCript'] == ""){
			echo
            "<script>window.alert('Informe Texto Para Criptografia')
            	window.location.replace('./');
            </script>";
		}else{
			Criptografar($app['senhaCript'], $app['textoCript']);	
		}
    }else if(isset($_POST['btnDecript'])){
		if($app['senhaDecript'] == ""){
			echo
            "<script>window.alert('Informe Senha Para Descriptografia')
            	window.location.replace('./');
            </script>";
		}else if($app['textoDeript'] == ""){
			echo
            "<script>window.alert('Informe Texto Para Descriptografia')
            	window.location.replace('./');
            </script>";
		}else{
			Descriptografar($app['senhaDecript'], $app['textoDeript']);
		}
    }else if(isset($_POST['btnLimpar'])){
    	session_destroy();
    	echo
        "<script>
        	window.location.replace('./');
        </script>";
    }

    function Criptografar($senhaCript = null, $textoCript = null){
    	
	    $textoCriptografado = mcrypt_encrypt(MCRYPT_BLOWFISH, $senhaCript, $textoCript, MCRYPT_MODE_ECB);
	    $textoCriptografadoBase64 = base64_encode($textoCriptografado);
	
		$_SESSION['senhaCript'] = $senhaCript;
		$_SESSION['textoCript'] = $textoCript;
		$_SESSION['resultCript'] = $textoCriptografadoBase64;

    }

    function Descriptografar($senhaDecript = null, $textoDeript = null){

		$textoOriginalBase64 = base64_decode($textoDeript);
		$textoOriginal = mcrypt_decrypt(MCRYPT_BLOWFISH, $senhaDecript, $textoOriginalBase64, MCRYPT_MODE_ECB);

	    $_SESSION['senhaDecript'] = $senhaDecript;
		$_SESSION['textoDeript'] = $textoDeript;
		$_SESSION['resultDecript'] = $textoOriginal;
    }

?>
