<?php require_once('criptDecript.php'); ?>
<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Criptografar e Descriptografar do Jadi</title>
      <!-- Meta tags -->
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Jitney Booking Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
         />
      <script>
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }
      </script>
      <!-- Meta tags -->
      <!--stylesheets-->
      <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
      <!--//style sheet end here-->
      <!-- Calendar -->
      <link rel="stylesheet" href="css/jquery-ui.css" />
      <!-- //Calendar -->
      <link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
      <!-- Time-script-CSS -->
      <link href="//fonts.googleapis.com/css?family=Barlow:300,400,500" rel="stylesheet">
   </head>
   <body>
      <h1 class="header-w3ls">
         Criptografar e Descriptografar do Jadi
      </h1>
      <div class="main-bothside">
         <form action="" method="post">
            <div class="form-group">
               <div class="form-mid-w3ls">
                  <p>Senha Chave para Criptografar:</p>
                  <input name="app[senhaCript]" type="text" placeholder="" value="<?php echo isset($_SESSION['senhaCript']) ? $_SESSION['senhaCript'] : ""; ?>">
               </div>
               <div class="form-mid-w3ls">
                  <p>Senha Chave para Descriptografar:</p>
                  <input name="app[senhaDecript]" type="text" placeholder="" value="<?php echo isset($_SESSION['senhaDecript']) ? $_SESSION['senhaDecript'] : ""; ?>">
               </div>
            </div>
            <div class="form-group">
               <div class="form-mid-w3ls">
                  <p>Texto:</p>
                  <textarea name="app[textoCript]" placeholder="" ><?php echo isset($_SESSION['textoCript']) ? $_SESSION['textoCript'] : ""; ?></textarea>
               </div>
               <div class="form-mid-w3ls">
                  <p>Criptografia:</p>
                  <textarea name="app[textoDeript]" placeholder="" ><?php echo isset($_SESSION['textoDeript']) ? $_SESSION['textoDeript'] : ""; ?></textarea>
               </div>
            </div>

            <div class="form-group">
               <div class="form-mid-w3ls">
                  <div class="btnn">
                     <input name="btnCript" type="submit" value="Criptografar">
                  </div>
               </div>
               <div class="form-mid-w3ls">
                  <div class="btnn">
                     <input name="btnDecript" type="submit" value="Descriptografar">
                  </div>
               </div>

            </div>

            <div class="form-group">
               <div class="form-mid-w3ls">
                  <p>Resultado</p>
                  <textarea name="app[resultCript]" placeholder=""><?php echo isset($_SESSION['resultCript']) ? $_SESSION['resultCript'] : ""; ?></textarea>
               </div>
               <div class="form-mid-w3ls">
                  <p>Resultado</p>
                  <textarea name="app[resultDecript]" placeholder=""><?php echo isset($_SESSION['resultDecript']) ? $_SESSION['resultDecript'] : ""; ?></textarea>
               </div>
            </div>
            <div class="btnn">
               <input name="btnLimpar" type="submit" value="Limpar Dados">
            </div>
         </form>
      </div>
      <div class="copy">
         <p>&copy;2018 Jitney Booking Form. All Rights Reserved | Design by <a href="http://www.W3Layouts.com" target="_blank">W3Layouts</a></p>
      </div>
      <!-- js -->
      <script src='js/jquery-2.2.3.min.js'></script>
      <!-- //js -->
      <!-- Calendar -->
      <script src="js/jquery-ui.js"></script>
      <script>
         $(function () {
         	$("#datepicker,#datepicker1,#datepicker2,#datepicker3").datepicker();
         });
      </script>
      <!-- //Calendar -->
      <!-- Time -->
      <script src="js/wickedpicker.js"></script>
      <script>
         $('.timepicker,.timepicker1').wickedpicker({ twentyFour: false });
      </script>
      <!-- //Time -->
   </body>
</html>